<?php
namespace PushON\Category\Block;

class Subcategories extends \Magento\Framework\View\Element\Template
{

    protected $_categoryCollectionFactory;

    protected $_registry;

    protected $_cacheType;

    /**
     * Header constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \PushON\Category\Model\Cache\Type $cacheType,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
    )
    {
        $this->_registry = $registry;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_cacheType = $cacheType;
        parent::__construct($context);
    }


    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    public function getSubCategories()
    {
        $cacheKey = "subcategory_cache_".$this->getCurrentCategory()->getId();
        try {
            if ($this->_cacheType->test($cacheKey)) {
                return json_decode($this->_cacheType->load($cacheKey));
            } else {
                $subCategoryIds = $this->getCurrentCategory()->getData('subcategories');

                $collection = $this->_categoryCollectionFactory->create()
                    ->addAttributeToSelect('*')
                    ->addFieldToFilter('entity_id', ['in' => $subCategoryIds])
                    ->addFieldToFilter('is_active', 1)
                    ->setOrder('position', 'ASC');

                $subCategories = array();

                foreach ($collection as $category) {
                    $subCategories[] = array(
                        "id"        =>   $category->getId(),
                        "name"      =>   $category->getName(),
                        "url"       =>   $category->getUrl(),
                        "image_url" =>   $category->getImageUrl(),
                        "icon_url"  =>   $category->getCategoryIcon()
                    );
                }

                $this->_cacheType->save(json_encode($subCategories), $cacheKey, [\PushON\Category\Model\Cache\Type::CACHE_TAG], 3600);

                return json_decode(json_encode($subCategories));
            }
        }
        catch(\Exception $e)
        {
            echo $e->getMessage();
            exit;
        }
    }


}