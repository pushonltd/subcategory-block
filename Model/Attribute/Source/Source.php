<?php

namespace PushON\Category\Model\Attribute\Source;

class Source extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    protected $_catalogConfig;

    protected $_categoryFactory;

    protected $_categoryModel;

    protected $_request;

    /**
     * Construct
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     */
    public function __construct(
        \Magento\Catalog\Model\Config $catalogConfig,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Catalog\Model\Category $categoryModel
    )
    {
        $this->_categoryModel = $categoryModel;
        $this->_catalogConfig = $catalogConfig;
        $this->_categoryFactory = $categoryFactory;
        $this->_request = $request;
    }

    /**
     * Retrieve Catalog Config Singleton
     *
     * @return \Magento\Catalog\Model\Config
     */
    protected function _getCatalogConfig()
    {
        return $this->_catalogConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllOptions()
    {

        $category_id = $this->_request->getParam('id');

        $parent_category = $this->_categoryModel->load($category_id);

        $children = $parent_category->getAllChildren();

        $_categories = $this->_categoryFactory->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', ['in' => $children]);

        $categories = array();
        foreach($_categories as $category)
        {
            if($category->getName() == $parent_category->getName())
            {
                continue;
            }

            $categories[] = array(
                "label"     =>  $category->getName(),
                "value"     =>  $category->getId()
            );
        }

        return $categories;
    }
}