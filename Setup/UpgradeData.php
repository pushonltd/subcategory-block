<?php
namespace PushON\Category\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    protected $_eavSetupFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->_eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade( ModuleDataSetupInterface $setup, ModuleContextInterface $context ) {
        $installer = $setup;

        $installer->startSetup();

        if(version_compare($context->getVersion(), '1.0.2', '<')) {
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);


            //create attribute
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'subcategories',
                [
                    'type'                              => 'text',
                    'label'                             => 'subcategories',
                    'input'                             => 'multiselect',
                    'source'                            => 'PushON\Category\Model\Attribute\Source\Source',
                    'backend'                           => 'PushON\Category\Model\Attribute\Backend\Backend',
                    'required'                          => false,
                    'sort_order'                        => 4,
                    'global'                            => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'wysiwyg_enabled'                   => true,
                    'is_html_allowed_on_front'          => true,
                    'group'                             => 'Content',
                ]
            );
        }

        $installer->endSetup();
    }

}